echo Starting Piper Server...
@echo off
set PIPER_DEBUG_MODE=0
set PIPER_SERVER_ROOT=%~dp0
call %PIPER_SERVER_ROOT%\venv\Scripts\activate.bat
python manage.py makemigrations
python manage.py migrate
echo Running Piper Server on Apache (Ctrl-C to stop)...
%PIPER_SERVER_ROOT%\resources\win32\Apache24\bin\httpd.exe -d %PIPER_SERVER_ROOT%\resources\win32\Apache24 -f %PIPER_SERVER_ROOT%\resources\win32\Apache24\conf\httpd.conf
echo Piper Server has been stopped.
pause