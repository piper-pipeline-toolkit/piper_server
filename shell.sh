#!/bin/bash
echo Starting Piper Server shell...
export PIPER_DEBUG_MODE="1"
export PIPER_SERVER_ROOT=$PWD
source venv/bin/activate
python manage.py makemigrations
python manage.py migrate
python manage.py shell