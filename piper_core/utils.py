# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
import getpass
import json
import re
from subprocess import call


def get_current_user():
    """
    Get the name of the current user.
    :return: the name of the current user (string);
    """
    return getpass.getuser()


def json_to_string(content):
    """
    Convert a dictionary object to a string.
    :param content: the content to convert to a string;
    :return: a string representation of a dictionary;
    """
    return json.dumps(content)


def string_to_json(string):
    """
    Convert a string to a dictionary.
    :param string: the string to convert;
    :return: a dictionary;
    """
    try:
        return json.loads(string.decode())
    except AttributeError:
        return json.loads(str(string))


def read_json(path):
    """
    Get the dictionary data contained in a file.
    :param path: the path to the file;
    :return: a dictionary;
    """
    with open(path) as dict_file:
        data = json.load(dict_file)
        return data


def write_json(dictionary, path):
    """
    Store a dictionary as a JSON file.
    :param dictionary: the dictionary to store;
    :param path: the path to the JSON file to write;
    :return: True
    """
    output = json.dumps(dictionary, sort_keys=True, indent=4, separators=(',', ': '))
    f = open(path, 'w+')
    f.write(output)
    f.close()
    return True


def environment_check(env_variable):
    """
    Check if an environment variable exists and if it has a valid value.
    :param env_variable: the name of the environment variable (string);
    :return: True if the environment variable exists and has a valid value, False if it doesn't.
    """
    if env_variable in os.environ and os.environ[env_variable]:
        return True
    return False


def browse_path(path):
    """
    Open a given path in the OS file browser.
    :param path: the (str) path to navigate to;
    :return: None;
    """
    if sys.platform == "win32":
        os.startfile(path)
    else:
        opener = "open" if sys.platform == "darwin" else "xdg-open"
        call([opener, path])


def timecode_match(timecode):
    """
    Check if a given string matches the timecode formatting convention.
    :param timecode: the (str) to evaluate;
    :return: True/False.
    """
    timecode_re = re.compile(r'^(?:(?:[0-1][0-9]|[0-2][0-3]):)(?:[0-5][0-9]:){2}(?:[0-2][0-9])$')
    if timecode_re.match(timecode):
        return True
    return False


def path_prefix_sanity_check(path):
    """
    Ensure that a path doesn't begin with a slash.
    :param path: a (str) path;
    :return: (str) path without slash;
    """
    if path and path[0] in ['\\', '/']:
        path = path[1:]
    return path


def path_suffix_sanity_check(path):
    """
    Ensure that a path doesn't end with a slash.
    :param path: a (str) path;
    :return: (str) path without slash;
    """
    if path and path[-1] in ['\\', '/']:
        path = path[:-1]
    return path
