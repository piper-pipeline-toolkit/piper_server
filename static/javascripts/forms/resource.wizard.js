var ingested_resource_url = false;

(function( $ ) {
	'use strict';
	/*
	Resource Ingestor Wizard
	*/
	var $resource_wizard_finish = $('#resource-wizard').find('ul.pager li.finish'),
		$resource_wizard_validator = $("#resource-wizard form").validate({
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
			$(element).remove();
		},
		errorPlacement: function( error, element ) {
			element.parent().append( error );
		}

	});

	function input_check(element) {
        $(element + '-exclamation').fadeOut();
        var value = $(element).val();
        if (!value) {
            $(element + '-exclamation').fadeIn();
        }
        return value;
    }

	$resource_wizard_finish.on('click', function( ev ) {
		ev.preventDefault();
		if (!ingested_resource_url) {
            var validated = $('#resource-wizard form').valid();
            if ( validated ) {
                $('#app-icon').fadeOut(function() {
                    $('#app-icon').removeClass('fa-file-picture-o').addClass('fa-spinner').addClass('fa-spin').fadeIn();
                });
                var data = new FormData($('#resource-ingestor-form').get(0));
                data.append('description',  $('#resource-description').val());
                data.append('resource_id',  $('#resource-select').val());
                data.append('resource_name',  $('#new-resource').val());
                data.append('notify',  $('#notify').val());
                data.append('csrfmiddlewaretoken', getCookie('csrftoken'));
                $.ajax({
                    url: '/Task/' + $('#task-select').val() + '/ingest/',
                    type: 'POST',
                    data: data,
                    success: function(json) {
                        $('#submit-btn a').fadeOut(function() {
                            $('#submit-btn a').html('<i class="fa fa-external-link-square"></i> Go to version').fadeIn();
                        });
                        ingested_resource_url = json['url'];
                        update_success(json);
                    },
                    error: update_error,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function() {
                    $('#app-icon').fadeOut(function() {
                        $('#app-icon').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-file-picture-o').fadeIn();
                    });
                });
            }
        } else {
            console.log(ingested_resource_url)
            return window.open(ingested_resource_url);
        }
	});

	$('#resource-wizard').bootstrapWizard({
		tabClass: 'wizard-steps',
		nextSelector: 'ul.pager li.next',
		previousSelector: 'ul.pager li.previous',
		firstSelector: null,
		lastSelector: null,
		onNext: function( tab, navigation, index, newindex ) {

			switch(index) {

                case 1:
                    if(!input_check("#project-select")) {
                        return false;
                    }

                    if ($("#shot-tab").hasClass("active")) {

                        if(!input_check("#sequence-select")) {
                            return false;
                        }

                        if(!input_check("#shot-select")) {
                            return false;
                        }

                    } else {
                        if(!input_check("#asset-type-select")) {
                            return false;
                        }

                        if(!input_check("#asset-select")) {
                            return false;
                        }
                    }
                    break;


                case 2:
                    if(!input_check("#step-select")) {
                        return false;
                    }

                    if(!input_check("#task-select")) {
                        return false;
                    }
                    break;

                case 3:
                    if ($('#resource-existing-radio').is(':checked')) {
                        $("#new-resource-exclamation").fadeOut();
                        if(!input_check("#resource-select")) {
                            return false;
                        }
                    } else {
                        $("#resource-select-exclamation").fadeOut();
                        if(!input_check("#new-resource")) {
                            return false;
                        }
                    }

                    break;
            }


            //var validated = $('#resource-wizard form').valid();
			//if( !validated ) {
			//	$resource_wizard_validator.focusInvalid();
			//	return false;
			//}

		},
		onTabChange: function( tab, navigation, index, newindex ) {
			var $total = navigation.find('li').size() - 1;
			$resource_wizard_finish[ newindex != $total ? 'addClass' : 'removeClass' ]( 'hidden' );
			$('#resource-wizard').find(this.nextSelector)[ newindex == $total ? 'addClass' : 'removeClass' ]( 'hidden' );
		},
		onTabShow: function( tab, navigation, index ) {
			var $total = navigation.find('li').length;
			var $current = index + 1;
			var $percent = ( $current / $total ) * 100;
			$('#resource-wizard').find('.progress-bar').css({ 'width': $percent + '%' });
		}
	});

}).apply( this, [ jQuery ]);
