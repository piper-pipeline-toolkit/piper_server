$(function() {
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  $(document).ready( function() {
      $(".sliding").toggle("slide");
      $(':file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        }
    });

  });

});

$('#breadcrumbs-nav-btn').on('click', function(event) {
    var next_breadcrumb = $('a.breadcrumb-link').last()[0];
    if (next_breadcrumb) {
        var fade_direction;
        if ($('#breadcrumbs-nav-icon').hasClass('fa-chevron-right')) {
            fade_direction = 'fadeInLeft';
        } else {
            fade_direction = 'fadeInRight';
        }
        $(this).addClass('appear-animation ' + fade_direction + ' appear-animation-visible');
        return next_breadcrumb.click();
    } else {
        $(this).addClass('appear-animation fadeInRight appear-animation-visible');
        document.location.href='/';
    }
});

$('.slide-toggle').on('click', function(event) {
  $("#" + $(this).data('slide-div')).toggle( "slide" );
});
