var selected_files = [];
var filters = [];
var selected_project;
var selected_user;
var selected_date;
var display_filter;
var element_index = 25;
var elements_loading = false;
var elements_maxed_out = false;

$( document ).ready(function() {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
    $('.selectpicker').selectpicker();
    $('.select2-container').select2();
});

(function( $ ) {

	/*
	Thumbnail: Select
	*/
	$('.mg-option input[type=checkbox]').on('change', function( ev ) {
		var wrapper = $(this).parents('.thumbnail');
		if($(this).is(':checked')) {
			wrapper.addClass('thumbnail-selected');
		} else {
			wrapper.removeClass('thumbnail-selected');
		}
	});

	$('.mg-option input[type=checkbox]:checked').trigger('change');

	/*
	Toolbar: Select All
	*/
	$('#mgSelectAll').on('click', function( ev ) {
		ev.preventDefault();
		var $this = $(this),
			$label = $this.find('> span');
			$checks = $('.mg-option input[type=checkbox]');

		if($this.attr('data-all-selected')) {
			$this.removeAttr('data-all-selected');
			$checks.prop('checked', false).trigger('change');
			$label.html($label.data('all-text'));
			$('#selection-icon').removeClass('fa-square');
			$('#selection-icon').addClass('fa-check-square');
		} else {
			$this.attr('data-all-selected', 'true');
			$checks.prop('checked', true).trigger('change');
			$label.html($label.data('none-text'));
			$('#selection-icon').removeClass('fa-check-square');
			$('#selection-icon').addClass('fa-square');
		}
	});

	/*
	Image Preview: Lightbox
	*/

	$('.thumb-preview .mg-zoom').on('click.lightbox', function( ev ) {
		$(this).closest('.thumb-preview').find('a.thumb-image').ekkoLightbox();
	});

	/*
	Thumnail: Dropdown Options
	*/
	$('.thumbnail .mg-toggle').parent()
		.on('show.bs.dropdown', function( ev ) {
			$(this).closest('.mg-thumb-options').css('overflow', 'visible');
		})
		.on('hidden.bs.dropdown', function( ev ) {
			$(this).closest('.mg-thumb-options').css('overflow', '');
		});

	$('.thumbnail').on('mouseenter', function() {
		var toggle = $(this).find('.mg-toggle');
		if ( toggle.parent().hasClass('open') ) {
			toggle.dropdown('toggle');
		}
	});

}(jQuery));

$('.selection-checkbox').change(function() {
    var id = $(this).prop( 'id' );
    if ($(this).is( ':checked' )) {
        selected_files.push(id);
    } else {
        selected_files.splice( $.inArray(id, selected_files), 1 );
    }
});

$('#create-playlist').on('click', function(event) {
    if (!$('#id_project').val()) {
        var notice = new PNotify({
            title: 'No project selected',
            text: 'You need to select a project to create a playlist...',
            type: 'error',
            addclass: 'stack-bottomright',
            stack: stack_bottom_right
        });
        return false;
    }

    if (selected_files.length) {
        $('#id_file_ids').val(selected_files.toString());
        $('#createFormTrigger').click();
    } else {
        var notice = new PNotify({
            title: 'Nothing selected',
            text: 'You need to select versions to create a playlist...',
            type: 'error',
            addclass: 'stack-bottomright',
            stack: stack_bottom_right
        });
        return false;
    }
});

$('#project-select').on('change', function() {
    var project_id = this.value;
    $checks = $('.mg-option input[type=checkbox]');
    $.each($checks, function( index, check ) {
        if ($(check).data('project-id') != project_id) {
            $(check).prop('checked', false).trigger('change');
        }
    });
    $('#id_project').val(project_id);
    selected_project = $("#project-select option:selected").text();
    filter_displayed();
});

$('#user-select').on('change', function() {
    selected_user = $("#user-select option:selected").text();
    filter_displayed();
});

$('#date-selector').datepicker().on('changeDate', function (ev) {
    var date = $(this).val().split('/');
    selected_date = date[2] + date[1] + date[0];
    filter_displayed();
});

$('#clear-date').click(function(){
    $('#date-selector').val('');
    selected_date = '';
    filter_displayed();
});

function filter_displayed() {
    var filter_classes = '.media-gallery-element';
    if (selected_project) {
        filter_classes += '.' + selected_project;
    }
    if (selected_user) {
        filter_classes += '.' + selected_user;
    }
    if (selected_date) {
        filter_classes += '.' + selected_date;
    }
    if (display_filter) {
        filter_classes += display_filter;
    }
    $.each(filters, function( index, filter ) {
        filter_classes +=  '.' + filter;
    });
    $('.media-gallery-element').each(function() {
        if ($(this).is(filter_classes)) {
            $(this).fadeIn();
        } else {
            $(this).fadeOut();
        }
    });
}

function add_remove_filter(action, filter) {
    if (action=='add') {
        filters.push(filter)
    } else {
        index = filters.indexOf(filter);
        if (index > -1) {
            filters.splice(index, 1);
        }
    }
    filter_displayed();
}

$('#tags-select').on('select2-selecting', function (event) {
    add_remove_filter('add', event.val);
});

$('#tags-select').on('select2-removing', function (event) {
    add_remove_filter('remove', event.val);
});

$('#display-filters').on('click', 'a', function(e) {
    e.preventDefault();
    var $this = $(this);
    display_filter = $this.attr("data-option-value");

    $('#media-gallery').find(".active").removeClass("active");
    $this.closest("li").addClass("active");

    filter_displayed();
});

$( document ).on('scroll', function() {
    if (elements_maxed_out) {
        return
    }
    if (elements_loading) {
        return
    }
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
        elements_loading = true;
        $('#loading-spinner').fadeIn();
        element_index += 25;
        var element_index_end = element_index + 25;
        $.ajax({
            url: '/media-gallery/elements/' + element_index + '/' + element_index_end + '/',
            type: 'GET',
            success: function(json) {
                $('#loading-spinner').fadeOut();
                if (json['data'] != '') {
                    $('#media-gallery-container').append(json['data']);
                } else {
                    elements_maxed_out = true;
                }
                elements_loading = false;
            }
        })
    }
});
