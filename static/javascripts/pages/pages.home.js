$('#search-form').on('submit', function(event) {
    event.preventDefault();
    return refresh_projects();
});

function refresh_projects() {
    $('#projects').fadeOut(function() {
        var url = '/projects/refresh/?q=' + $('#search-query').val();
        $.ajax({
            url: url,
            type: 'GET',
            success: function(json) {
                $('#projects').fadeOut(function() {
                    $('#projects').html(json['data']).fadeIn();
                });
            }
        });
    });
    return false;
}
