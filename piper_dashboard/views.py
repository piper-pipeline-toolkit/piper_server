# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
import json
import xlwt
import datetime
import time
import string
import random
from shutil import copyfile, rmtree
from itertools import chain
from django.shortcuts import render, redirect, HttpResponse, HttpResponseRedirect
from django.template import loader
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout, login, authenticate
from django.core.files.storage import FileSystemStorage
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.conf import settings
from django.http import JsonResponse
from django.contrib import messages
from django.contrib.auth.views import auth_login
from django.utils.timezone import make_aware
from django.core.exceptions import FieldError
from piper_core.models import *
from piper_core.api import DbApi
from piper_core.config import DATE_TIME_FORMAT
from piper_core.config.database import PARENT_TO_CHILDREN
from .forms import *
from .utils import spreadsheet_to_dictionaries

CONFIGURABLE_ENTITIES = ['Project', 'Episode', 'Sequence', 'Shot', 'Asset', 'Task', 'Resource', 'Version', 'File']


def sign_in(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                auth_login(request, user)
                return HttpResponseRedirect(request.POST.get('next', '/'))
        else:
            messages.error(request, 'Username or password incorrect...')
            return redirect('login')

    return render(request, 'pages-signin.html', context={'next': request.GET.get('next', '/')})


def recover(request):
    return render(request, 'pages-recover-password.html')


def reset_password(request):
    email = request.POST.get('email')
    user = User.objects.filter(email=email).first()
    if not email or not user:
        return JsonResponse({'status': 0,
                             'message': 'Could not find a valid user with the e-mail: {}'.format(email)},
                            status=500)

    db_api = DbApi()
    if not db_api.globals.email_ready:
        return JsonResponse({'status': 0, 'message': 'Piper is not configured to send email yet...'}, status=500)

    new_password = "".join([string.printable[random.randint(0, len(string.printable) - 1)] for i in range(12)])
    user.set_password(new_password)
    user.save()
    message = "Your password has been successfully reset to: {} " \
              "\nPlease update your password once you've logged back into Piper.".format(new_password)
    db_api.notify(target=email, subject='Piper | Password Reset', message=message)
    return JsonResponse({'status': 1,
                         'message': 'New password sent to {}'.format(email)},
                        status=200)


@login_required
def home(request):
    context = {'create_form': ProjectForm(), 'refresh_div': 'projects'}
    if request.user.is_superuser:
        context['projects'] = Project.objects.all().order_by('-created')
    else:
        context['projects'] = request.user.projects.order_by('-created')
    return render(request, 'pages-home.html', context=context)


@login_required
def projects_refresh(request):
    context = {'entity_model': 'Project'}
    if request.user.is_superuser:
        projects = Project.objects.all().order_by('-created')
    else:
        projects = request.user.projects.order_by('-created')

    query = request.GET.get('q')
    if query:
        projects = projects.filter(Q(name__icontains=query) |
                                   Q(long_name__icontains=query) |
                                   Q(description__icontains=query)).distinct()

    context['projects'] = projects

    data = loader.render_to_string('projects.html', context=context)
    return HttpResponse(
        json.dumps({'status': 1, 'data': data}),
        content_type='application/json'
    )


@login_required
def activity(request, entity_model, entity_id, index_start, index_end):
    db_api = DbApi()
    entity = eval(entity_model).objects.get(id=entity_id)
    activities = db_api.get_entity_activity(entity=entity, index_start=index_start, index_end=index_end)
    data = None
    if activities:
        data = loader.render_to_string('timeline-activities.html',
                                       context={'entity': entity,
                                                'activities': activities,
                                                'refresh': True})
    return HttpResponse(
        json.dumps({'data': data}),
        content_type='application/json'
    )


@login_required
def browser(request):
    if request.user.is_superuser:
        projects = Project.objects.all()
    else:
        projects = request.user.projects.all()
    return render(request, 'pages-browser.html', context={'asset_types': AssetType.objects.all(),
                                                          'statuses': Status.objects.all(),
                                                          'projects': projects,
                                                          'sidebar_collapsed': True,
                                                          'data_tree': {}})


@login_required
def task_manager(request):
    if request.user.is_superuser:
        projects = Project.objects.all()
    else:
        projects = request.user.projects.all()
    return render(request, 'pages-task-manager.html', context={'projects': projects,
                                                               'asset_types': AssetType.objects.all(),
                                                               'create_form': TaskManagerForm(),
                                                               'sidebar_collapsed': True})


@login_required
def task_manager_create(request):
    if request.method == 'POST':
        form = TaskManagerForm(request.POST)
        if form.is_valid():
            form.cleaned_data['created_by'] = request.user.username
            db_api = DbApi()
            instance, message = db_api.create_entity(entity_type='Task', **form.cleaned_data)

            if not instance:
                return JsonResponse({'status': 0,
                                     'message': message},
                                    status=500)

            db_api.email_notification_check(entity=instance, request=request, action='created')

            return HttpResponse(
                json.dumps({'entity': instance.jsonify(),
                            'status': 1,
                            'message': 'Created {0} {1}'.format(instance.type, instance.name)}),
                content_type='application/json'
            )
        else:
            return JsonResponse({'status': 0,
                                 'message': json.dumps(form.errors)},
                                status=500)


@login_required
def retime_task(request, task_id, start_time, end_time):
    if request.method == 'POST':
        task = Task.objects.get(id=task_id)
        if task.created_by != request.user.username:
            if not request.user.is_superuser:
                return JsonResponse({'status': 0,
                                     'message': 'Unauthorised action'},
                                    status=500)
        task.start_date = make_aware(datetime.datetime.strptime(request.POST.get('start_time'), "%d-%m-%Y %H:%M"))
        task.end_date = make_aware(datetime.datetime.strptime(request.POST.get('end_time'), "%d-%m-%Y %H:%M"))
        task.save()
        return JsonResponse({'status': 1,
                             'message': 'Task {} start/end time updated'.format(task.long_name)},
                            status=200)


@login_required
def resource_ingestor(request):
    if request.user.is_superuser:
        projects = Project.objects.all()
    else:
        projects = request.user.projects.all()
    return render(request, 'pages-resource-ingestor.html', context={'asset_types': AssetType.objects.all(),
                                                                    'projects': projects,
                                                                    'sidebar_collapsed': True,
                                                                    'steps': Step.objects.filter(
                                                                        level='shot'),
                                                                    'users': User.objects.all(),
                                                                    'file_upload_form': AttachmentForm()
                                                                    })


@login_required
def browser_tree(request, project_name):
    if not project_name:
        return HttpResponse(
            json.dumps({'status': 1, 'data': {}}),
            content_type='application/json'
        )

    project = Project.objects.get(name=project_name)

    if request.user not in project.user_set.all():
        if project.created_by != request.user.username:
            if not request.user.is_superuser:
                return JsonResponse({'status': 0,
                                     'message': 'Unauthorised action'},
                                    status=500)
    db_api = DbApi()
    tree = db_api.project_tree(project)
    data = loader.render_to_string('browser-tree.html', context={'data_tree': tree})
    return HttpResponse(
        json.dumps({'status': 1, 'data': data}),
        content_type='application/json'
    )


@login_required
def browser_details(request, entity_model, entity_id):
    entity = eval(entity_model).objects.get(id=entity_id)
    if entity.created_by != request.user.username:
        if not request.user.is_superuser:
            return JsonResponse({'status': 0,
                                 'message': 'Unauthorised action'},
                                status=500)
    details = loader.render_to_string('browser-details.html', context={'entity': entity})
    return HttpResponse(
        json.dumps({'status': 1, 'data': details}),
        content_type='application/json'
    )


@login_required
def entity_field_value(request, entity_model, entity_id, field_name):
    entity = eval(entity_model).objects.get(id=entity_id)
    value = entity.safe_get(field_name)
    if hasattr(value, 'long_name'):
        value = value.long_name
    return HttpResponse(
        json.dumps({'status': 1, 'data': value}),
        content_type='application/json'
    )


@login_required
def user(request, department_id, user_id):
    instance = User.objects.get(id=user_id)

    is_current_user = True
    if request.user.id != user_id:
        is_current_user = False

    db_api = DbApi()
    activities = db_api.get_entity_activity(entity=instance)
    context = {
        'is_current_user': is_current_user,
        'activities': activities,
        'selected_user': instance,
        'form': UserProfileForm(instance=instance, initial={'department': instance.department}),
        'sidebar_collapsed': True
    }
    return render(request, 'pages-user-profile.html', context)


@login_required
def lock(request):
    if request.user.is_authenticated:
        user = request.user.jsonify()['fields']
        thumbnail = request.user.thumbnail.url
        form = CustomLoginForm()
        redirect_url = request.META.get('HTTP_REFERER')
        logout(request)
        context = {'form': form, 'current_user': user, 'thumbnail': thumbnail, 'redirect_url': redirect_url}
        return render(request, 'pages-lock-screen.html', context)

    return redirect('/')


def unlock(request):
    if request.method == 'POST':
        form = CustomLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            account = authenticate(username=username, password=password)
            if account is not None:
                login(request, account)
                return HttpResponseRedirect(request.POST.get('next'))
    return redirect('/')


@login_required
def lock_entity(request, entity_model, entity_id):
    if request.method == 'POST':
        return toggle_entity_locked(request, entity_model, entity_id, locked=True)


@login_required
def unlock_entity(request, entity_model, entity_id):
    if request.method == 'POST':
        return toggle_entity_locked(request, entity_model, entity_id, locked=False)


def toggle_entity_locked(request, entity_model, entity_id, locked):
    if not request.user.is_superuser:
        return JsonResponse({'status': 0,
                             'message': 'Unauthorised action'},
                            status=500)
    entity = eval(entity_model).objects.get(id=entity_id)
    entity.locked = locked
    entity.save()
    action = 'Unlocked'
    if locked:
        action = 'Locked'
    return HttpResponse(
        json.dumps({'status': 1, 'entity': entity.jsonify(), 'message': '{0} {1}'.format(action, entity)}),
        content_type='application/json'
    )


@login_required
def studio_config_systems(request, config_id=None):
    if not request.user.is_human or not request.user.is_superuser:
        return handler403(request)
    if request.method == 'POST':
        system = System.objects.get(id=config_id)
        form = SystemForm(request.POST, instance=system)
        if form.is_valid():
            form.save()
            return HttpResponse(
                json.dumps({'entity': form.instance.jsonify(),
                            'status': 1,
                            'message': 'Updated {0} {1}'.format(form.instance.type, form.instance.long_name)}),
                content_type='application/json'
            )
        else:
            return JsonResponse({'status': 0,
                                 'message': json.dumps(form.errors)},
                                status=500)
    else:
        systems = System.objects.all()
        forms = []
        for system in systems:
            forms.append(SystemForm(instance=system))
        return render(request, 'pages-studio-config-systems.html', {'forms': forms, 'sidebar_collapsed': True})


@login_required
def studio_config_globals(request):
    if not request.user.is_human or not request.user.is_superuser:
        return handler403(request)
    globals_config = GlobalsConfig.objects.get(id=1)
    if request.method == 'POST':
        form = GlobalsConfigForm(request.POST, instance=globals_config)
        if form.is_valid():
            form.save()
            return HttpResponse(
                json.dumps({'entity': form.instance.jsonify(),
                            'status': 1,
                            'message': 'Updated studio globals'}),
                content_type='application/json'
            )
        else:
            return JsonResponse({'status': 0,
                                 'message': json.dumps(form.errors)},
                                status=500)
    else:
        form = GlobalsConfigForm(instance=globals_config)
        return render(request, 'pages-studio-config-globals.html', {'form': form, 'sidebar_collapsed': True})


@login_required
def software(request, software_id=None):
    if not request.user.is_human or not request.user.is_superuser:
        return handler403(request)

    if software_id:
        software = Software.objects.get(id=software_id)
        db_api = DbApi()
        activities = db_api.get_entity_activity(entity=software)
        return render(request, 'pages-timeline.html', {'entity': Software.objects.get(id=software_id),
                                                       'activities': activities,
                                                       'users': User.objects.filter(is_superuser=True),
                                                       'children': ['Module'],
                                                       'sidebar_collapsed': True,
                                                       'extend_page': 'index.html',
                                                       'edit_form': _entity_form(instance=software),
                                                       'file_upload_form': AttachmentForm()})

    return render(request, 'pages-studio-config-software.html', {'entities': Software.objects.all(),
                                                                 'entity_model': 'Software',
                                                                 'full_url': True,
                                                                 'configurable': False,
                                                                 'sidebar_collapsed': True,
                                                                 'create_form': SoftwareForm(),
                                                                 'dummy': Software(),
                                                                 'parent': None})


@login_required
def module(request, software_id=None, module_id=None):
    if not request.user.is_human or not request.user.is_superuser:
        return handler403(request)

    if module_id:
        module = Module.objects.get(id=module_id)
        db_api = DbApi()
        activities = db_api.get_entity_activity(entity=module)
        return render(request, 'pages-timeline.html', {'entity': module,
                                                       'activities': activities,
                                                       'users': User.objects.filter(is_superuser=True),
                                                       'children': [],
                                                       'sidebar_collapsed': True,
                                                       'extend_page': 'index.html',
                                                       'edit_form': _entity_form(instance=module),
                                                       'file_upload_form': AttachmentForm()})

    if software_id:
        software = Software.objects.get(id=software_id)
        modules = Module.objects.filter(software=software)
        parent = software
    else:
        modules = Module.objects.all()
        parent = None
    return render(request, 'pages-studio-config-modules.html', {'entities': modules,
                                                                'entity_model': 'Module',
                                                                'full_url': True,
                                                                'configurable': False,
                                                                'create_form': ModuleForm(),
                                                                'sidebar_collapsed': True,
                                                                'dummy': Module(),
                                                                'parent': parent})


@login_required
def profile(request, profile_id=None):
    if not request.user.is_human or not request.user.is_superuser:
        return handler403(request)

    if profile_id:
        profile = Profile.objects.get(id=profile_id)
        db_api = DbApi()
        activities = db_api.get_entity_activity(entity=profile)
        return render(request, 'pages-timeline.html', {'entity': profile,
                                                       'activities': activities,
                                                       'users': User.objects.filter(is_superuser=True),
                                                       'children': [],
                                                       'sidebar_collapsed': True,
                                                       'extend_page': 'index.html',
                                                       'edit_form': _entity_form(instance=profile),
                                                       'file_upload_form': AttachmentForm()})

    return render(request, 'pages-studio-config-profiles.html', {'entities': Profile.objects.all(),
                                                                 'entity_model': 'Profile',
                                                                 'full_url': True,
                                                                 'configurable': False,
                                                                 'sidebar_collapsed': True,
                                                                 'create_form': ProfileForm(),
                                                                 'dummy': Profile(),
                                                                 'parent': None})


@login_required
def configure(request, entity_model, entity_id):
    if request.method == 'POST':
        entity = eval(entity_model).objects.get(id=entity_id)
        if entity.created_by != request.user.username:
            if not request.user.is_superuser:
                return JsonResponse({'status': 0,
                                     'message': 'Unauthorised action'},
                                    status=500)
        config_rules = string_to_json(request.POST.get('config_rules'))
        data = dict()
        for key, value in config_rules.items():
            if value.isdigit():
                value = float(value)
            data[key] = value
        if 'priority' in data and not request.user.is_superuser:
            return JsonResponse({'status': 0,
                                 'message': 'You do not have authority to configure priority.'},
                                status=500)
        db_api = DbApi()
        db_api.configure(entity=entity, data=data)
        return HttpResponse(
            json.dumps({'status': 1,
                        'message': 'Configured {0} {1}'.format(entity.type, entity.long_name)}),
            content_type='application/json'
        )


@login_required
def create_entity(request, entity_model, _parent_model=None, _parent_id=None):
    if request.method == 'POST':
        if entity_model == 'Task':
            if _parent_model == 'Asset':
                form = AssetTaskForm
            else:
                form = ShotTaskForm
        elif _parent_model and _parent_model == 'Episode':
            if entity_model == 'Asset':
                form = EpisodeAssetForm
            else:
                form = EpisodeSequenceForm
        elif entity_model == 'User':
            form = UserCreationForm
        else:
            form = MODELS_TO_FORMS[entity_model]

        form = form(request.POST)

        if form.is_valid():
            if _parent_id:
                form.cleaned_data[_parent_model.lower()] = eval(_parent_model).objects.get(id=_parent_id)
                form.cleaned_data.pop('{}_id'.format(_parent_model.lower()), None)
            form.cleaned_data['created_by'] = request.user.username

            db_api = DbApi()
            instance, message = db_api.create_entity(entity_type=entity_model, **form.cleaned_data)

            if not instance:
                return JsonResponse({'status': 0,
                                     'message': message},
                                    status=500)

            if entity_model == 'Project':
                request.user.projects.add(instance)
                request.user.save()
            elif entity_model == 'User':
                instance.set_password(form.cleaned_data["password"])
                instance.save()
            else:
                pass

            db_api.email_notification_check(entity=instance, request=request, action='created')

            return HttpResponse(
                json.dumps({'entity': instance.jsonify(),
                            'status': 1,
                            'message': 'Created {0} {1}'.format(instance.type, instance.name)}),
                content_type='application/json'
            )
        else:
            return JsonResponse({'status': 0,
                                 'message': json.dumps(form.errors)},
                                status=500)


@login_required
def department(request, department_id=None):
    if department_id:
        db_api = DbApi()
        department = Department.objects.get(id=department_id)
        activities = db_api.get_entity_activity(entity=department)
        users = User.objects.filter(is_superuser=True)
        users |= department.user_set.all()
        form = MODELS_TO_FORMS['Department'](instance=department)
        return render(request, 'pages-timeline.html', {'entity': department,
                                                       'activities': activities,
                                                       'users': users.distinct(),
                                                       'children': PARENT_TO_CHILDREN['Department'],
                                                       'sidebar_collapsed': True,
                                                       'extend_page': 'index.html',
                                                       'edit_form': _entity_form(instance=department),
                                                       'file_upload_form': AttachmentForm()})
    if not request.user.is_human or not request.user.is_superuser:
        return handler403(request)
    return render(request, 'pages-studio-config-departments.html', {'entities': Department.objects.all(),
                                                                    'entity_model': 'Department',
                                                                    'create_form': DepartmentForm(),
                                                                    'configurable': False,
                                                                    'sidebar_collapsed': True,
                                                                    'dummy': Department(),
                                                                    'parent': None})


@login_required
def studio_config_users(request):
    if not request.user.is_human or not request.user.is_superuser:
        return handler403(request)
    return render(request, 'pages-studio-config-users.html', {'entities': User.objects.all(),
                                                              'entity_model': 'User',
                                                              'full_url': True,
                                                              'create_form': UserCreationForm(),
                                                              'configurable': False,
                                                              'sidebar_collapsed': True,
                                                              'dummy': User(),
                                                              'parent': None})


@login_required
def studio_config_production(request):
    if not request.user.is_human or not request.user.is_superuser:
        return handler403(request)
    data = {'Status': {'entities': Status.objects.all(), 'create_form': StatusForm(), 'dummy': Status()},
            'Category': {'entities': Category.objects.all(), 'create_form': CategoryForm(), 'dummy': Category()},
            'Step': {'entities': Step.objects.all(), 'create_form': StepForm(), 'dummy': Step()},
            'AssetType': {'entities': AssetType.objects.all(), 'create_form': AssetTypeForm(), 'dummy': AssetType()},
            'Tag': {'entities': Tag.objects.all(), 'create_form': TagForm(), 'dummy': Tag()},
            'EditType': {'entities': EditType.objects.all(), 'create_form': EditTypeForm(), 'dummy': EditType()}}
    return render(request, 'pages-studio-config-production.html', {'data': data,
                                                                   'full_url': True,
                                                                   'sidebar_collapsed': True,
                                                                   'parent': None})


@login_required
def users(request, department_id):
    return render(request, 'pages-entities-table.html',
                  {'entities': User.objects.filter(department_id=department_id),
                   'entity_model': 'User',
                   'configurable': False,
                   'sidebar_collapsed': True,
                   'dummy': User(),
                   'parent': Department.objects.get(id=department_id)})


@login_required
def history(request, _parent_model, _parent_id):
    changes = History.objects.filter(_parent_id=_parent_id, _parent_model=_parent_model).order_by('-created')
    return render(request, 'pages-history.html', {'changes': changes, 'sidebar_collapsed': True,})


@login_required
def project(request, project_id=None):
    if project_id:
        project_entity = Project.objects.get(id=project_id)
        if request.user.is_superuser or project_entity in request.user.projects.all():
            users = User.objects.filter(is_superuser=True)
            users |= project_entity.user_set.all()
            if project_entity.episodic:
                children = PARENT_TO_CHILDREN['Project']
            else:
                children = ['Sequence', 'Asset']
            db_api = DbApi()
            activities = db_api.get_entity_activity(entity=project_entity)
            return render(request, 'pages-timeline.html', {'entity': project_entity,
                                                           'activities': activities,
                                                           'users': users.distinct(),
                                                           'artists': User.objects.filter(is_human=True),
                                                           'edit_form': _entity_form(instance=project_entity),
                                                           'file_upload_form': AttachmentForm(),
                                                           'children': children,
                                                           'sidebar_collapsed': True,
                                                           'extend_page': 'index.html',
                                                           'configurable': True})
    if request.user.is_superuser:
        entities = Project.objects.all()
    else:
        entities = request.user.projects.all()
    return render(request, 'pages-entities-table.html', {'entities': entities,
                                                         'entity_model': 'Project',
                                                         'configurable': True,
                                                         'create_form': ProjectForm(),
                                                         'sidebar_collapsed': True,
                                                         'dummy': Project(),
                                                         'parent': None})


def _project_permission_check(request, project_id):
    try:
        project_entity = Project.objects.get(id=project_id)
    except ObjectDoesNotExist:
        return False
    if not request.user.is_superuser:
        if project_entity not in request.user.projects.all():
            return False
    return True


@login_required
def entities(request, **kwargs):
    if not _project_permission_check(request, kwargs['project_id']):
            return render(request, 'pages-no-permission.html')
    if '_parent_id' in kwargs:
        _parent_id = kwargs['_parent_id']
        _parent_model = kwargs['_parent_model']
    else:
        _parent_id = kwargs['project_id']
        _parent_model = 'Project'

    parent = eval(_parent_model).objects.get(id=_parent_id)
    initial = dict()
    initial['{}_id'.format(parent.type.lower())] = parent.id
    entity_model = kwargs['entity_model']

    # Account for hierarchical difference for episodic sequences and tasks:
    if entity_model == 'Task':
        if kwargs['_parent_model'] == 'Asset':
            form = AssetTaskForm
        else:
            form = ShotTaskForm
    elif '_parent_model' in kwargs and kwargs['_parent_model'] == 'Episode':
        if entity_model == 'Asset':
            form = EpisodeAssetForm
        else:
            form = EpisodeSequenceForm
    else:
        form = MODELS_TO_FORMS[kwargs['entity_model']]

    form = form(initial=initial)
    entity = eval(entity_model)

    return render(request, 'pages-entities-table.html',
                  {'entities': entity.objects.filter(_parent_id=_parent_id, _parent_model=_parent_model),
                   'entity_model': entity_model,
                   'create_form': form,
                   'configurable': True,
                   'sidebar_collapsed': True,
                   'dummy': entity(),
                   'parent': parent})


@login_required
def entity(request, **kwargs):
    return _timeline(request=request, extend_page='index.html', **kwargs)


@login_required
def timeline(request, **kwargs):
    return _timeline(request,  extend_page='bare_bones.html', **kwargs)


def _timeline(request, extend_page, **kwargs):
    if not _project_permission_check(request, kwargs['project_id']):
        return render(request, 'pages-no-permission.html')
    entity_model = kwargs['entity_model']
    instance = eval(entity_model).objects.get(id=kwargs['entity_id'])
    users = User.objects.filter(is_superuser=True)
    users |= instance.project.user_set.all()
    if entity_model == 'Task':
        children = ['Resource']
    elif entity_model in PARENT_TO_CHILDREN:
        children = PARENT_TO_CHILDREN[entity_model]
    else:
        children = []
    db_api = DbApi()
    activities = db_api.get_entity_activity(entity=instance)
    timeline_only = not extend_page.startswith('index')

    # Try to find a preview for this entity:
    preview = None
    if entity_model != 'File':
        try:
            preview_filters = {entity_model.lower(): instance, 'is_preview': True}
            preview = File.objects.filter(**preview_filters).last()
        except FieldError:
            pass

    return render(request, 'pages-timeline.html',
                  {'entity': instance,
                   'activities': activities,
                   'users': users.distinct(),
                   'file_upload_form': AttachmentForm(),
                   'children': children,
                   'sidebar_collapsed': True,
                   'extend_page': extend_page,
                   'timeline_only': timeline_only,
                   'edit_form': _entity_form(instance=instance),
                   'preview': preview,
                   'configurable': kwargs['entity_model'] in CONFIGURABLE_ENTITIES})


@login_required
def export_entities_table_xls(request, entity_model, _parent_model=None, _parent_id=None):
    parent = None
    if _parent_id and _parent_model:
        parent = eval(_parent_model).objects.get(id=_parent_id)
        entities = eval(entity_model).objects.filter(_parent_model=_parent_model, _parent_id=_parent_id)
    else:
        entities = eval(entity_model).objects.all()
    if not entities:
        return HttpResponse('')

    response = HttpResponse(content_type='application/ms-excel')
    timestamp = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
    if parent:
        content_disposition = 'attachment; filename="piper_{0}_{1}s_{2}.xls"'.format(parent.name.lower(),
                                                                                     entity_model.lower(),
                                                                                     timestamp)
    else:
        content_disposition = 'attachment; filename="piper_{0}s_{1}.xls"'.format(entity_model.lower(), timestamp)
    response['Content-Disposition'] = content_disposition
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet(entity_model)

    row_num = 0
    col_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    for key in entities.first().display_fields.keys():
        if key not in ['Created', 'Updated', 'Created by', 'Updated by']:
            ws.write(row_num, col_num, key, font_style)
            col_num += 1

    font_style = xlwt.XFStyle()

    for entity in entities:
        row_num += 1
        col_num = 0
        for label, data in entity.display_fields.items():
            if label not in ['Created', 'Updated', 'Created by', 'Updated by']:
                ws.write(row_num, col_num, data['value'], font_style)
                col_num += 1

    wb.save(response)
    return response


@login_required
def spreadsheet_resource_ingestor_template(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="piper_resource_ingestor_template.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Piper Resource Ingestor')

    row_num = 0
    col_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    for key in ['Project', 'Episode', 'Sequence', 'Shot', 'Asset', 'Step', 'Task', 'Resource', 'Files', 'Description',
                'Thumbnail']:
        ws.write(row_num, col_num, key, font_style)
        col_num += 1

    wb.save(response)
    return response


@login_required
def update_entities_table_xls(request, entity_model, _parent_model=None, _parent_id=None):
    if request.method == 'POST' and request.FILES['spreadsheet']:
        db_api = DbApi()
        spreadsheet = request.FILES['spreadsheet']
        file_system = FileSystemStorage()
        filename = file_system.save(spreadsheet.name, spreadsheet)
        uploaded_file_url = file_system.url(filename)
        uploaded_file_path = settings.BASE_DIR + uploaded_file_url
        parent = None
        if _parent_model and _parent_id:
            parent = db_api.get_entity(entity_type=_parent_model, id=_parent_id)
        result = db_api.batch_update_entities(entity_model=entity_model,
                                              parent=parent,
                                              request=request,
                                              entities_dict_data=spreadsheet_to_dictionaries(
                                                  spreadsheet=uploaded_file_path))
        if os.path.exists(uploaded_file_path):
            os.remove(uploaded_file_path)
        return HttpResponse(
            json.dumps({'status': 1,
                        'warnings': result['warnings'],
                        'message': 'Batch updated {}s'.format(entity_model.lower())}),
            content_type='application/json'
        )

    return HttpResponse(
        json.dumps({'status': 0,
                    'message': 'Batch update failed...'}),
        content_type='application/json'
    )


@login_required
def batch_ingest_from_xls(request):
    if request.method == 'POST' and request.FILES['spreadsheet']:
        db_api = DbApi()
        spreadsheet = request.FILES['spreadsheet']
        file_system = FileSystemStorage()
        filename = file_system.save(spreadsheet.name, spreadsheet)
        uploaded_file_url = file_system.url(filename)
        uploaded_file_path = settings.BASE_DIR + uploaded_file_url
        dict_data = spreadsheet_to_dictionaries(spreadsheet=uploaded_file_path)
        result = db_api.batch_ingest_entities(entities_dict_data=dict_data, request=request)
        if os.path.exists(uploaded_file_path):
            os.remove(uploaded_file_path)
        return HttpResponse(
            json.dumps({'status': 1,
                        'warnings': result['warnings'],
                        'message': 'Batch ingestion from spreadsheet done'}),
            content_type='application/json'
        )

    return HttpResponse(
        json.dumps({'status': 0,
                    'message': 'Batch ingestion from spreadsheet failed...'}),
        content_type='application/json'
    )


@login_required
def update_thumbnail(request, entity_model, entity_id):
    if request.method == 'POST' and request.FILES['thumbnail_file']:
        thumbnail_file = request.FILES['thumbnail_file']
        file_system = FileSystemStorage()
        filename = file_system.save('thumbnails/' + thumbnail_file.name, thumbnail_file)
        entity = eval(entity_model).objects.get(id=entity_id)
        previous_thumbnail = entity.thumbnail.url
        entity.thumbnail = filename
        entity.user_set_thumbnail = True
        entity.save()
        data = loader.render_to_string('thumbnail.html', context={'entity': entity})
        if os.path.exists(previous_thumbnail):
            os.remove(previous_thumbnail)
        return HttpResponse(
            json.dumps({'status': 1,
                        'data': data,
                        'message': 'Thumbnail updated'}),
            content_type='application/json'
        )

    return HttpResponse(
        json.dumps({'status': 0,
                    'message': 'Thumbnail update failed...'}),
        content_type='application/json'
    )


@login_required
def post_note(request, _parent_model, _parent_id, reply_to_id=None):
    if request.method == 'POST':
        content = request.POST.get('content')
        if not content:
            return JsonResponse({'status': 0,
                                 'message': 'Note is empty...'},
                                status=500)
        warnings = []
        note = Note.objects.create(content=content,
                                   author=request.user,
                                   _parent_model=_parent_model,
                                   _parent_id=_parent_id)

        if '@' in content:
            words = content.split()
            for word in words:
                if word.startswith('@'):
                    user_name = word.replace('@', '')
                    try:
                        user = User.objects.get(username=user_name)
                    except ObjectDoesNotExist:
                        warnings.append('User {} is invalid, ignoring mention.'.format(user_name))
                        continue
                    note.mentioned_users.add(user)

        tag_names = request.POST.getlist('tags')
        if tag_names:
            for tag_name in tag_names[0].split(','):
                if tag_name:
                    try:
                        tag = Tag.objects.get(name=tag_name.lower())
                    except ObjectDoesNotExist:
                        tag = Tag.objects.create(name=tag_name.lower(), created_by=request.user.name)
                        tag.save()
                    note.tags.add(tag)

        if reply_to_id:
            reply_to = Note.objects.get(id=reply_to_id)
            note.reply_to = reply_to
            note.mentioned_users.add(reply_to.author)

        note.save()

        form = AttachmentForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            for file in request.FILES.getlist('file'):
                attachment = Attachment(file=file, author=request.user, _parent_model=note.type, _parent_id=note.id)
                attachment.save()

        return HttpResponse(
            json.dumps({'status': 1,
                        'warnings': warnings,
                        'message': 'Note posted!'}),
            content_type='application/json'
        )


@login_required
def ingest_resource(request, task_id):
    if request.method == 'POST':
        description = request.POST.get('description')
        resource_id = request.POST.get('resource_id')
        resource_name = request.POST.get('resource_name')
        notify = request.POST.get('notify')
        db_api = DbApi()
        task = Task.objects.get(id=task_id)
        warnings = []

        if resource_id:
            resource = Resource.objects.get(id=resource_id)
        else:
            resource, msg = db_api.create_entity(entity_type='Resource',
                                                 task=task,
                                                 name=resource_name,
                                                 artist=request.user,
                                                 description=description)
            if not resource:
                return JsonResponse({'status': 0,
                                     'message': msg},
                                    status=500)

            db_api.email_notification_check(entity=resource, request=request, action='created')

        version, msg = db_api.create_entity(entity_type='Version',
                                            resource=resource,
                                            artist=request.user,
                                            description=description)
        if not version:
            return JsonResponse({'status': 0,
                                 'message': msg},
                                status=500)

        db_api.email_notification_check(entity=version, request=request, action='created')

        form = AttachmentForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            files_tmp_root = 'tmp/{}/'.format(str(time.time()).replace('.', ''))
            file_system = FileSystemStorage()
            files = request.FILES.getlist('file')
            uploaded_files = []
            for file in files:
                filename = file_system.save(files_tmp_root + file.name, file)
                uploaded_file_url = file_system.url(filename)
                uploaded_files.append(settings.BASE_DIR + uploaded_file_url)

            processed_files = db_api.process_files(uploaded_files)

            for file_path in processed_files:
                is_sequence = processed_files[file_path]['is_sequence']
                file, msg = db_api.create_entity(entity_type='File',
                                                 format=file_path.split('.')[-1],
                                                 artist=request.user,
                                                 is_sequence=is_sequence,
                                                 version=version)

                if not file:
                    warnings.append('File not published: {}'.format(msg))
                    continue

                publish_path = os.path.join(db_api.system.publish_root, file.publish_path)
                publish_dir = os.path.dirname(publish_path)
                if not os.path.isdir(publish_dir):
                    os.makedirs(publish_dir)

                thumbnail = None
                if is_sequence:
                    frame = db_api.get_start_frame(file_entity=file)
                    for element in processed_files[file_path]['elements']:
                        copyfile(element, publish_path.replace('.####.', '.{}.'.format(frame)))
                        frame += 1
                    if lements and file.is_image:
                        thumbnail = elements[len(elements)/2]
                else:
                    copyfile(file_path, publish_path)
                    if not thumbnail and file.is_image:
                        thumbnail = publish_path

                if thumbnail:
                    if os.path.isfile(thumbnail):
                        db_api.update_thumbnail(entity=file.version, thumbnail=thumbnail)
            if files:
                rmtree(os.path.join(settings.MEDIA_ROOT, files_tmp_root))

        if notify:
            subject = 'Piper | {0} | Ingested resource {1} version {2}'.format(task.project.long_name,
                                                                               resource.long_name,
                                                                               version.long_name)
            message = 'Resource {0} version {1} has been successfully ingested by user {2}.'.format(resource.long_name,
                                                                                             version.long_name,
                                                                                             request.user.username)
            message += '\nURL: {0}\nPath: {1}'.format(version.full_url, version.publish_path)
            warnings += db_api.notify(target=notify, subject=subject, message=message)

        return HttpResponse(
            json.dumps({'status': 1,
                        'warnings': warnings,
                        'url': version.full_url,
                        'message': 'Resource version ingested!'}),
            content_type='application/json'
        )


@login_required
def delete(request, entity_model, entity_id):
    db_api = DbApi()
    entity = eval(entity_model).objects.get(id=entity_id)
    if entity.created_by != request.user.username:
        if not request.user.is_superuser:
            return JsonResponse({'status': 0,
                                 'message': 'Unauthorised action'},
                                status=500)
    if hasattr(entity, 'locked') and entity.locked:
        return JsonResponse({'status': 0,
                             'message': 'Entity locked'},
                            status=500)
    db_api.email_notification_check(entity=entity, request=request, action='deleted')
    entity_name = entity.name
    entity.delete()
    return HttpResponse(
        json.dumps({'status': 1, 'message': 'Deleted {0} {1}'.format(entity_model, entity_name)}),
        content_type='application/json'
    )


@login_required
def table_refresh(request, entity_model, _parent_model=None, _parent_id=None):
    if _parent_model and _parent_id:
        entities = eval(entity_model).objects.filter(_parent_model=_parent_model, _parent_id=_parent_id)
    else:
        entities = eval(entity_model).objects.all()

    new_data = []
    entity_ids = []
    for entity in entities:
        entity_ids.append('{0}/{1}/'.format(entity.type, entity.id))
        row = []
        for key, data in entity.display_fields.items():
            if key == 'Thumbnail':
                value = loader.render_to_string('tables-editable-thumbnail.html',
                                                context={'entity': entity, 'data': data})
            else:
                value = loader.render_to_string('tables-editable-cell.html',
                                                context={'entity': entity, 'data': data})
            row.append(value)
        row.append(loader.render_to_string('tables-editable-actions.html',
                                           context={'entity': entity,
                                                    'configurable': entity_model in CONFIGURABLE_ENTITIES}))
        new_data.append(row)

    return HttpResponse(
        json.dumps({'data': new_data, 'entity_ids': entity_ids}),
        content_type='application/json'
    )


@login_required
def timeline_refresh(request, entity_model, entity_id):
    entity = eval(entity_model).objects.get(id=entity_id)
    db_api = DbApi()
    activities = db_api.get_entity_activity(entity=entity)
    timeline = loader.render_to_string('timeline.html',
                                       context={'entity': entity,
                                                'activities': activities,
                                                'refresh': True})
    return HttpResponse(
        json.dumps({'data': timeline}),
        content_type='application/json'
    )


@login_required
def note_thread_refresh(request, note_id):
    note = Note.objects.get(id=note_id)
    note_thread = loader.render_to_string('notes-correspondence.html',
                                          context={'note': note,
                                                   'file_upload_form': AttachmentForm()})
    return HttpResponse(
        json.dumps({'data': note_thread}),
        content_type='application/json'
    )


@login_required
def note_trash(request, note_id):
    note = Note.objects.get(id=note_id)
    note.trashed_by.add(request.user)
    note.save()

    return HttpResponse(
        json.dumps({'status': 1, 'entity': note.jsonify(), 'message': 'Note {0} moved to trash.'.format(note.id)}),
        content_type='application/json'
    )


def _entity_form(instance):
    entity_model = instance.type
    initial = {}
    if instance.parent:
        initial['{}_id'.format(instance.parent.type.lower())] = instance.parent.id

    # Account for hierarchical difference for episodic sequences and tasks:
    if entity_model == 'Task':
        if instance.parent.type == 'Asset':
            form = AssetTaskForm
        else:
            form = ShotTaskForm
    elif instance.parent and instance.parent.type == 'Episode':
        if entity_model == 'Asset':
            form = EpisodeAssetForm
        else:
            form = EpisodeSequenceForm
    else:
        form = MODELS_TO_FORMS[entity_model]

    return form(instance=instance, initial=initial)


@login_required
def entity_form(request, entity_model, entity_id):
    instance = eval(entity_model).objects.get(id=entity_id)
    form = _entity_form(instance=instance)
    data = loader.render_to_string('forms-entity.html',
                                   context={'entity': instance, 'form': form})
    return HttpResponse(
        json.dumps({'data': data}),
        content_type='application/json'
    )


@login_required
def log_work(request, task_id):
    task = Task.objects.get(id=task_id)
    db_api = DbApi()
    duration = request.POST.get('duration')
    work_log, msg = db_api.create_entity(entity_type='WorkLog',
                                         task=task,
                                         _parent_id=task.id,
                                         _parent_model='Task',
                                         name=str(timezone.now().strftime(DATE_TIME_FORMAT)),
                                         duration=float(duration),
                                         description=request.POST.get('description'),
                                         artist=request.user)
    if not work_log:
        return JsonResponse({'status': 0, 'message': msg}, status=500)
    return JsonResponse({'status': 1,
                         'message': 'Logged {0} hours of work for task {1}'.format(duration, task.long_name),
                         'log': work_log.jsonify()},
                        status=200)


@login_required
def update_entity(request, entity_model, entity_id):
    if request.method == 'POST':
        db_api = DbApi()
        instance = eval(entity_model).objects.get(id=entity_id)
        if hasattr(instance, 'locked') and instance.locked:
            return JsonResponse({'status': 0,
                                 'message': 'Entity locked'},
                                status=500)
        if entity_model == 'Task':
            if instance.link.type == 'Asset':
                form = AssetTaskForm
            else:
                form = ShotTaskForm
        else:
            form = MODELS_TO_FORMS[entity_model]
        form = form(request.POST, instance=instance)
        if form.is_valid():
            instance = form.save()
            db_api.email_notification_check(entity=instance, request=request)
            instance.updated_by = request.user.username
            instance.save()
            return HttpResponse(
                json.dumps({'entity': form.instance.jsonify(),
                            'status': 1,
                            'message': 'Updated {0} {1}'.format(instance.type, instance.long_name)}),
                content_type='application/json'
            )
        else:
            return JsonResponse({'status': 0,
                                 'message': json.dumps(form.errors)},
                                status=500)


@login_required
def notes_inbox(request):
    return render(request,
                  'pages-mailbox-folder.html',
                  context={'notes': request.user.mentions.exclude(trashed_by__in=[request.user]),
                           'tags': Tag.objects.all().order_by('name'),
                           'sidebar_collapsed': True,
                           'inbox': True})


def _get_notes(request, note_type, note_filter):
    if note_type == 'trash':
        notes = Note.objects.filter(author=request.user, trashed_by__in=[request.user])
    elif note_type == 'sent':
        notes = Note.objects.filter(author=request.user).exclude(trashed_by__in=[request.user])
    else:
        notes = request.user.mentions.exclude(trashed_by__in=[request.user])

    if note_filter:
        if note_filter == 'read':
            notes = notes.filter(seen_by__in=[request.user])
        elif note_filter == 'unread':
            notes = notes.exclude(seen_by__in=[request.user])
        else:
            pass

    return notes


@login_required
def notes_refresh(request, note_type=None, note_filter=None):
    notes = _get_notes(request, note_type, note_filter)

    data = loader.render_to_string('mailbox-notes.html', context={'notes': notes})

    return HttpResponse(
        json.dumps({'data': data}),
        content_type='application/json'
    )


@login_required
def search_notes(request, note_type=None, note_filter=None):
    notes = _get_notes(request, note_type, note_filter)
    query = request.GET.get('q')
    if query:
        notes = notes.filter(Q(content__icontains=query)).order_by('-created')

    notes = [note for note in notes if note.project and _project_permission_check(request, note.project.id)]

    data = loader.render_to_string('mailbox-notes.html', context={'notes': notes, 'sidebar_collapsed': True})

    return HttpResponse(
        json.dumps({'data': data}),
        content_type='application/json'
    )


@login_required
def notes_outbox(request):
    notes = Note.objects.filter(author=request.user).exclude(trashed_by__in=[request.user])
    return render(request, 'pages-mailbox-folder.html', context={'notes': notes,
                                                                 'tags': Tag.objects.all().order_by('name'),
                                                                 'sidebar_collapsed': True,
                                                                 'sent': True})


@login_required
def notes_trash(request):
    return render(request, 'pages-mailbox-folder.html', context={'notes': Note.objects.filter(
                                                                 author=request.user,
                                                                 trashed_by__in=[request.user]),
                                                                 'tags': Tag.objects.all().order_by('name'),
                                                                 'sidebar_collapsed': True,
                                                                 'trash': True})


@login_required
def mail_correspondence(request, note_id):
    note = Note.objects.get(id=note_id)
    if not (note.project and _project_permission_check(request, note.project.id)):
        return render(request, 'pages-no-permission.html')
    if request.user in note.mentioned_users.all():
        if request.user not in note.seen_by.all():
            note.seen_by.add(request.user)
            note.save()
    users = User.objects.filter(is_superuser=True)
    users |= note.project.user_set.all()
    return render(request, 'pages-mailbox-correspondence.html', context={'note': note,
                                                                         'tags': Tag.objects.all().order_by('name'),
                                                                         'users': users.distinct(),
                                                                         'file_upload_form': AttachmentForm(),
                                                                         'sidebar_collapsed': True})


@login_required
def jsonify(request, entity_model, entity_id):
    entity = eval(entity_model).objects.get(id=entity_id)
    if entity.project and not _project_permission_check(request, entity.project.id):
        return JsonResponse({'status': 0,
                             'message': 'Unauthorised action'},
                            status=500)
    return HttpResponse(
        json.dumps({'status': 1, 'data': entity.jsonify()}),
        content_type='application/json'
    )


@login_required
def search_everywhere(request):
    return render(request, 'pages-search-results.html', context=_get_search_context(request))


def _get_search_context(request):
    query = request.GET.get('q')

    # Filter projects based on the user:
    if request.user.is_superuser:
        projects = Project.objects
    else:
        projects = request.user.projects

    # Find tags:
    tags = Tag.objects.filter(name__contains=query)
    tagged = []

    # Find projects:
    results = projects.filter(Q(name__icontains=query) |
                              Q(long_name__icontains=query) |
                              Q(description__icontains=query)).distinct()

    # Find users and departments:
    for entity_model in ['User', 'Department']:
        entities = eval(entity_model).objects.filter(Q(name__icontains=query) |
                                                     Q(long_name__icontains=query) |
                                                     Q(description__icontains=query)).distinct()
        results = sorted(chain(results, entities), key=lambda instance: instance.created, reverse=True)

    # Find everything else:
    notes = []
    attachments = []
    for project in projects.all():
        for entity_model in ['Episode', 'Sequence', 'Shot', 'Asset', 'Task', 'Resource', 'Version']:
            project_entities = eval(entity_model).objects.filter(project=project)
            for tag in tags:
                tagged += list(project_entities.filter(tags__in=[tag]))

            entities = project_entities.filter(Q(name__icontains=query) |
                                               Q(long_name__icontains=query) |
                                               Q(description__icontains=query)).distinct()
            results = sorted(chain(results, entities), key=lambda instance: instance.created, reverse=True)

        # Find notes:
        project_notes = Note.objects.filter(_project=project).order_by('-created')
        matching_notes = [note for note in project_notes.filter(content__icontains=query)
                          if note.project and _project_permission_check(request, note.parent.id)]

        # Merge notes:
        notes = sorted(chain(notes, matching_notes), key=lambda instance: instance.created, reverse=True)

        # Filter tagged notes:
        tagged_notes = []
        for tag in tags:
            tagged_notes += [note for note in project_notes if tag in note.tags.all()]

        # Merge tagged:
        tagged = sorted(chain(tagged, tagged_notes), key=lambda instance: instance.created, reverse=True)

        # Find attachments:
        project_attachments = Attachment.objects.filter(project=project).order_by('-created')
        matching_attachments = [x for x in project_attachments.filter(file__icontains=query)
                                if x.project and _project_permission_check(request, x.project.id)]

        # Merge notes:
        attachments = sorted(chain(attachments, matching_attachments), key=lambda instance: instance.created,
                             reverse=True)

    context={'results': results,
             'notes': notes,
             'attachments': attachments,
             'query': query,
             'tagged': tagged}

    return context


@login_required
def search_refresh(request):
    data = loader.render_to_string('search-results.html', context=_get_search_context(request))
    return HttpResponse(
        json.dumps({'status': 1, 'data': data}),
        content_type='application/json'
    )


@login_required
def media_gallery(request):
    files = File.objects.filter(Q(is_image=True) | Q(is_video=True),
                                is_sequence=False,
                                project__in=request.user.projects.all())
    playlists = Playlist.objects.filter(project__in=request.user.projects.all())
    results = sorted(chain(files, playlists), key=lambda instance: instance.created, reverse=True)[:25]
    return render(request,
                  'pages-media-gallery.html',
                  context={'entities': results,
                           'tags': Tag.objects.all(),
                           'users': User.objects.filter(is_human=True),
                           'sidebar_collapsed': True,
                           'create_form': PlaylistForm()})


@login_required
def media_gallery_elements(request, index_start=0, index_end=25):
    files = File.objects.filter(Q(is_image=True) | Q(is_video=True),
                                is_sequence=False,
                                project__in=request.user.projects.all())
    playlists = Playlist.objects.filter(project__in=request.user.projects.all())
    results = sorted(chain(files, playlists),
                     key=lambda instance: instance.created, reverse=True)[index_start:index_end]
    data = ''
    if results:
        data = loader.render_to_string('media-elements.html', context={'entities': results})
    return HttpResponse(
        json.dumps({'status': 1, 'data': data}),
        content_type='application/json'
    )


@login_required
def display_video(request, file_id):
    file = File.objects.get(id=file_id)
    return render(request,
                  'display-video.html',
                  context={'entity': file})


def handler403(request, exception=None):
    return render(request, 'pages-403.html', locals())


def handler404(request, exception=None):
    return render(request, 'pages-404.html', locals())


def handler500(request, exception=None):
    return render(request, 'pages-500.html', locals())


def handler502(request, exception=None):
    return render(request, 'pages-502.html', locals())


def get_user_platform(request):
    if 'Macintosh' in request.META['HTTP_USER_AGENT']:
        platform = 'darwin'
    elif 'Windows' in request.META['HTTP_USER_AGENT']:
        platform = 'win32'
    else:
        platform = 'linux'
    return platform


def profile_software(request, profile_id):
    platform = get_user_platform(request)
    profile = Profile.objects.get(id=profile_id)
    software = [app for app in profile.software.all() if app.system.platform == platform]
    return render(request, 'profile-software.html', context={'software': software})


@login_required
def add_artist_project(request, username, project_id):
    if request.method == 'POST':
        project = Project.objects.get(id=project_id)
        user = User.objects.get(username=username)
        user.projects.add(project)
        user.save()
        return JsonResponse({'status': 1,
                             'message': 'Artist {0} added to project {1}'.format(username, project.long_name)},
                            status=200)


@login_required
def remove_artist_project(request, username, project_id):
    if request.method == 'POST':
        project = Project.objects.get(id=project_id)
        if not (request.user.is_superuser or project.created == request.user.username):
            return JsonResponse({'status': 0,
                                 'message': 'Unauthorised action'},
                                status=500)

        user = User.objects.get(username=username)
        user.projects.remove(project)
        user.save()
        return JsonResponse({'status': 1,
                             'message': 'Artist {0} removed from project {1}'.format(username, project.long_name)},
                            status=200)


@login_required
def page_redirect(request, entity_model, entity_id):
    entity = eval(entity_model).objects.get(id=entity_id)
    return redirect(entity.full_url)
