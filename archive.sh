#!/bin/bash
echo Archiving Piper Server...
source venv/bin/activate
python manage.py archive
echo Piper Server has been archived.